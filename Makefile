NAME = malfter42/node
REVISION = `cat ./VERSION.txt`

.PHONY: build build-nocache tag-latest push push-latest release git-tag-version

build:
	docker build -t $(NAME)-8:$(REVISION) --build-arg NODE_IMAGE_VERSION=8 --rm ./image
	docker build -t $(NAME)-13:$(REVISION) --build-arg NODE_IMAGE_VERSION=13 --rm ./image

build-nocache:
	docker build -t $(NAME)-8:$(REVISION) --build-arg NODE_IMAGE_VERSION=8 --no-cache --rm ./image
	docker build -t $(NAME)-13:$(REVISION) --build-arg NODE_IMAGE_VERSION=13 --no-cache --rm ./image

tag-latest:
	docker tag $(NAME)-8:$(REVISION) $(NAME):latest
	docker tag $(NAME)-13:$(REVISION) $(NAME):latest

push:
	docker push $(NAME)-8:$(REVISION)
	docker push $(NAME)-13:$(REVISION)

push-latest:
	docker push $(NAME)-8:latest
	docker push $(NAME)-13:latest

release: build tag-latest push push-latest

git-tag-version: release
	git tag -a $(REVISION) -m "Revision $(REVISION)"
	git push origin $(REVISION)
	./bumpVersion.sh
	git commit VERSION.txt -m "bump version"
	git push origin
