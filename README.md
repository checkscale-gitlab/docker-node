# docker-node

[![pipeline status](https://gitlab.com/malfter/docker-node/badges/master/pipeline.svg)](https://gitlab.com/malfter/docker-node/-/commits/master)

![logo](./logo.png)

In this project different node versions are provided as docker images.

## Quick Start

Run Node docker container:
```bash
$ docker run malfter/node-8:latest
v8.17.0

$ docker run malfter/node-13:latest
v13.11.0
```
## Build Images

### CI-Job

Please refer to: [.gitlab-ci.yml](.gitlab-ci.yml)

### Manual

Build images local run:
```bash
$ make build
```

## Changelog

Please refer to: [CHANGELOG.md](CHANGELOG.md)
