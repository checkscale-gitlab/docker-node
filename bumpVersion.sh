#!/usr/bin/env bash

set -e

old_version=$(cat ./VERSION.txt)  
new_version=$(expr $old_version + 1)
sed -i "s/$old_version\$/$new_version/g" VERSION.txt 
