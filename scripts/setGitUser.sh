#!/usr/bin/env bash

set -e

: "${GIT_USER_NAME?Need to set GIT_USER_NAME}"
: "${GIT_USER_EMAIL?Need to set GIT_USER_EMAIL}"

git config --global user.name "${GIT_USER_NAME}"
git config --global user.email "${GIT_USER_EMAIL}"